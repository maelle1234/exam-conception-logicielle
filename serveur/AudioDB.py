import requests
import random
import healthpy.requests

class AudioDB:
    """
    Classe qui permet d'effectuer des requêtes spécifiques à l'API AudioDB
    """

    def check_api(self):
        """
        Méthode permettant de vérifier le bon fonctionnement de l'API AudioDB
        """
        url_test = 'https://www.theaudiodb.com/api/v1/json/2/search.php?s={}'.format("Rick Astley")
        r = requests.get(url_test)
        status, checks = healthpy.requests.check("test", url_test)
        check = {"status": status, "code":r.status_code}
        return check

    def get_artistid(self, artistname):
        """
        Méthode qui permet de récupérer l'identifiant d'un artiste en fonction de son nom
        """
        payload = {"s" : artistname}
        r = requests.get('https://www.theaudiodb.com/api/v1/json/2/search.php', params = payload).json()
        #on récupère le contenu de la requête
        artistid = int(r["artists"][0]["idArtist"])
        return artistid

    def get_artist_albumids(self, artistid):
        """
        Méthode qui permet de récupérer les identifiants d'album d'un artiste en fonction de son id
        """
        payload = {"i" : artistid}
        r = requests.get('https://www.theaudiodb.com/api/v1/json/2/album.php', params = payload).json()
        albumids = []
        for album in r["album"]:
            albumids.append(int(album["idAlbum"]))
        # liste d'entiers qui correspondent aux id de tous les albums d'un artiste
        return albumids

    def get_album_tracknames(self, albumid):
        """
        Méthode qui permet de récupérer les noms de chansons d'un album en fonction de son id
        """
        payload = {"m" : albumid}
        r = requests.get('https://www.theaudiodb.com/api/v1/json/2/track.php', params = payload).json()
        tracknames = []
        for track in r["track"]:
            tracknames.append({"title":track["strTrack"],"id":track["idTrack"]})
        # liste de dict de tous les titres des chansons présentes dans l'album + id
        return tracknames # liste de dictionnaires

    def get_track_mv(self, artistid, trackid):
        """
        Méthode qui permet de récupérer l'url de la vidéo suggérée pour une chanson 
        en fonction de l'id de l'artiste et du nom de la chanson
        """
        payload = {"i" : artistid}
        r = requests.get('https://www.theaudiodb.com/api/v1/json/2/mvid.php', params = payload).json()
        mv_url = ""
        if r["mvids"] is not None:
            for mv in r["mvids"]:
                if mv["idTrack"] == trackid:
                    mv_url = mv["strMusicVid"]
        return mv_url

    def get_random_song_from_artistname(self, artistname):
        """
        Méthode qui permet d'obtenir au hasard le nom d'une chanson d'un artiste en fonction
        du nom de l'artiste 
        """
        artistid = self.get_artistid(artistname)
        albumids = self.get_artist_albumids(artistid)
        random_albumid = albumids[random.randint(0, len(albumids)-1)]
        # on génère un entier aléatoire entre 0 et le nombre d'albums de l'artiste - 1
        # on récupère l'id de album correspondant
        tracknames = self.get_album_tracknames(random_albumid)
        random_track = tracknames[random.randint(0, len(tracknames)-1)]
        # on récupère le nom de la chanson sélectionnée aléatoirement
        return random_track # dictionnaire avec le titre et l'id de la chanson
