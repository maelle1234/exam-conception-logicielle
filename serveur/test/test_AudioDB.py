from unittest import TestCase
from serveur.AudioDB import AudioDB

class TestAudioDB(TestCase):

    def test_get_artistid(self):
        a = AudioDB()
        rick_id = a.get_artistid("Rick Astley")
        self.assertEqual(type(rick_id), int)
        # on vérifie qu'on obtient bien un nombre entier
        # on pourrait vérifier qu'on obtient bien l'id exact de rick astley
        # mais si un jour AudioDB décide de changer ses id le test ne marche plus
        # alors que pourtant ça n'a aucun impact sur le reste du code
