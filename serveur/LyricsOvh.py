import requests
import healthpy.requests

class LyricsOvh:
    """
    Classe qui permet d'effectuer des requêtes spécifiques à l'API LyricsOvh
    """

    def check_api(self):
        """
        Méthode permettant de vérifier le bon fonctionnement de l'API LyricsOvh
        """
        url_test = 'https://api.lyrics.ovh/v1/{}/{}'.format("Rick Astley", "Never gonna give you up")
        r = requests.get(url_test)
        status, checks = healthpy.requests.check("test", url_test)
        check = {"status": status, "code":r.status_code}
        return check

    def get_lyrics(self, artist, title):
        """
        Méthode qui permet d'obtenir les paroles d'une chanson en fonction de son titre et
        du nom de l'artiste
        """ 
        title = title.replace("/", "") # si / dans le nom de la chanson : requête http beug
        # amélioration: traiter d'autres symboles qui semblent occasioner des beugs
        r = requests.get('https://api.lyrics.ovh/v1/{}/{}'.format(artist, title)).json()
        lyrics = ""
        if "lyrics" in r.keys():
            lyrics = r["lyrics"]
        return lyrics

