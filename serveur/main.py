import uvicorn
from fastapi import FastAPI
from serveur.AudioDB import AudioDB
from serveur.LyricsOvh import LyricsOvh

app = FastAPI()

@app.get("/")
async def get_check_everything():
    a = AudioDB()
    l = LyricsOvh()
    return {"dependencies": {
                "AudioDB API": a.check_api(),
                "LyricsOvh API": l.check_api()}
            }

@app.get("/random/{artist_name}")
async def get_song_info(artist_name):
    a = AudioDB()
    l = LyricsOvh()
    artist_id = a.get_artistid(artist_name)
    random_title = a.get_random_song_from_artistname(artist_name) #dict
    #print(random_title)
    suggested_mv = a.get_track_mv(artist_id, random_title["id"])
    lyrics = l.get_lyrics(artist_name, random_title["title"]) 
    return {"artist" : artist_name,
            "title" : random_title["title"],
            "suggested_youtube_url" : suggested_mv,
            "lyrics" : lyrics}
