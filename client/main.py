import requests
import json
import os
from dotenv import load_dotenv
from random import randint
from karaoke_checks import Karaoke_checks

load_dotenv()
server_url = os.getenv('SERVER_URL')

kc = Karaoke_checks()

with open('client/rudy.json') as json_data:
    rudy_artists = json.load(json_data)

# on choisit d'ignorer les notes
l_artists = [artist["artiste"] for artist in rudy_artists]

playlist = []
nb_songs = 20

while len(playlist) < nb_songs:
    # choisir un artiste au hasard
    random_artist = l_artists[randint(0,len(l_artists)-1)]
    # on fait la requête à notre webservice
    r = requests.get('{}/random/{}'.format(server_url, random_artist)).json()
    if kc.has_lyrics(r): # on vérifie qu'on a bien les paroles pour le karaoke
        playlist.append(r)

print(playlist)

# idée amélioration : si pas de vidéo ne pas ajouter la chanson non plus
# méthode has_video de Karaoke_checks déjà codée mais 
# si peu de vidéos pour les artistes donnés que c'était trop long et occasionait 
# des beugs

# autre idée : améliorer l'affichage de la playlist, dataframe panda ou bien sauvegarde d'un
# fichier json
