from unittest import TestCase
from client.karaoke_checks import Karaoke_checks

class TestKaraokeChecks(TestCase):

    def test_has_lyrics(self):
        song = {"artist": "rick astley",
                "title": "What You See Is What You Don't Get",
                "suggested_youtube_url": "",
                "lyrics": "Don't know what you've got, not till you lose it\r\nYou had my love but you only abused it\r\nI've had enough of all your excuses and lies\r\nYou can look but don't you touch\r\nYou Played around girl, much too much\r\nYou had it all but you gave it up\n\nIt's too late\n\n\n\nWhat you see is what you don't get\n\nYou had your chance and then you blew it\n\nI was the one you never Knew it\n\nNow it's time to forget\n\nWhat you see is what you can't have\n\nYou're missing out and that is too bad\n\nI was the one who had the last laugh\n\nAnd all you've got is Regret\n\n\n\nI made up my mind, showed your true colours\n\nOne of a Kind? no you're just like the others\n\nI'm saying goodbye to all your excuses and lies\n\nAnd now there's nothing left to say\n\nSo why are you standing in my way?\n\nAnd don't go begging me to stay, it's too late\n\n\n\nWhat you see is what you don't get\n\nYou had your chance and then you blew it\n\nI was the one you never Knew it\n\nNow it's time to forget\n\nWhat you see is what you can't have\n\nYou're missing out and that is too bad\n\nI was the one who had the last laugh\n\nAnd all you've got is Regret\n\n\n\nWhat you see is what you don't get\n\nAre you a fool or have you got it yet?\n\nYou had it all and then you threw it all away"
                }
        kc = Karaoke_checks()
        self.assertTrue(kc.has_lyrics(song))

    def test_has_video(self):
        song = {"artist": "rick astley",
                "title": "What You See Is What You Don't Get",
                "suggested_youtube_url": "",
                "lyrics": "Don't know what you've got, not till you lose it\r\nYou had my love but you only abused it\r\nI've had enough of all your excuses and lies\r\nYou can look but don't you touch\r\nYou Played around girl, much too much\r\nYou had it all but you gave it up\n\nIt's too late\n\n\n\nWhat you see is what you don't get\n\nYou had your chance and then you blew it\n\nI was the one you never Knew it\n\nNow it's time to forget\n\nWhat you see is what you can't have\n\nYou're missing out and that is too bad\n\nI was the one who had the last laugh\n\nAnd all you've got is Regret\n\n\n\nI made up my mind, showed your true colours\n\nOne of a Kind? no you're just like the others\n\nI'm saying goodbye to all your excuses and lies\n\nAnd now there's nothing left to say\n\nSo why are you standing in my way?\n\nAnd don't go begging me to stay, it's too late\n\n\n\nWhat you see is what you don't get\n\nYou had your chance and then you blew it\n\nI was the one you never Knew it\n\nNow it's time to forget\n\nWhat you see is what you can't have\n\nYou're missing out and that is too bad\n\nI was the one who had the last laugh\n\nAnd all you've got is Regret\n\n\n\nWhat you see is what you don't get\n\nAre you a fool or have you got it yet?\n\nYou had it all and then you threw it all away"
                }
        kc = Karaoke_checks()
        self.assertFalse(kc.has_video(song))