class Karaoke_checks:
    """ 
    Classe qui permet de réaliser des vérifications utiles pour une chanson donnée
    dans l'objectif d'un karaoke
    """

    def has_lyrics(self, song):
        # song est un dictionnaire (ce qu'on récupère avec le webservice)
        return (len(song["lyrics"]) != 0)

    def has_video(self, song):
        # song est un dictionnaire (ce qu'on récupère avec le webservice)
        return (len(song["suggested_youtube_url"]) != 0)