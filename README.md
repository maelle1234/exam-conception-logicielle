# Examen de conception logicielle

L'objectif de ce projet est de réaliser un webservice permettant d'afficher des informations sur un titre choisi au hasard pour un artiste donné. Pour obtenir les informations nécessaires, les API AudioDB et LyricsOvh sont utilisées. On trouve également un client qui réalise le scénario suivant: à partir d'un fichier donné comptenant des noms d'artistes, réaliser une playlist pour un karaoke.

Ce dépôt est donc organisé en deux parties, une partie serveur pour la mise en place du webservice et une partie client pour l'implémentation du scénario client.

## Quickstart

Cloner le dépôt git
```
git clone https://gitlab.com/maelle1234/exam-conception-logicielle
cd exam-conception-logicielle
```

Avant toute chose, ne pas oublier de compléter le fichier .env situé dans le dossier client avec l'URL du serveur que vous souhaitez utiliser! Par défaut, le serveur devrait être http://localhost:8000.

Installer les dépendances du projet côté client
```
pip install -r client/requirements.txt
```
Attention! Il semblerait qu'il y ait une incompatibilité entre certains packages nécessaires et une version de python antérieure à la 3.6 (requests, fastapi ou uvicorn par exemple), merci donc d'utiliser une version de python adéquate :)

Installer les dépendances du projet côté serveur
```
pip install -r serveur/requirements.txt
```

Lancer les tests unitaires
```
python -m unittest
```

Lancer le webservice
```
uvicorn serveur.main:app --reload
```

Lancer le scénario client depuis un nouveau terminal
```
python client/main.py
```

## Fonctionnement du webservice

Pour consulter l'état de santé des dépendances du webservice une fois lancé
```
{url du serveur}/
```

Pour obtenir des informations sur un titre choisi au hasard pour un artiste donné, Harry Styles par exemple
```
{url du serveur}/random/harry%20styles
```

Pour obtenir la documentation complète du webservice
```
{url du serveur}/docs
```

## Schéma d'architecture 

```mermaid
graph TD;
Client(Client)
Serveur(Serveur)
LyricsOvh[API LyricsOvh]
AudioDB[API AudioDB]
Serveur -- requête HTTP --> LyricsOvh
Serveur -- requête HTTP --> AudioDB
AudioDB -- réponse .json --> Serveur
LyricsOvh -- réponse .json --> Serveur
Client -- requête HTTP --> Serveur
Serveur -- réponse .json --> Client
```